package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;

import com.tsystems.javaschool.tasks.MathUtils;

public class Calculator {

    private static final int DIGITS_AFTER_DOT = 4;
    private static final Map<Character, Integer> SIMBOLS_PRECEDENCE;
    private static final Map<Character, BiFunction<Double, Double, Double>> OPERATIONS;
    static {
        SIMBOLS_PRECEDENCE = new HashMap<>();
        SIMBOLS_PRECEDENCE.put('(', 0);
        SIMBOLS_PRECEDENCE.put('*', 2);
        SIMBOLS_PRECEDENCE.put('/', 2);
        SIMBOLS_PRECEDENCE.put('+', 1);
        SIMBOLS_PRECEDENCE.put('-', 1);

        OPERATIONS = new HashMap<>();
        OPERATIONS.put('+', (a, b) -> a + b);
        OPERATIONS.put('-', (a, b) -> a - b);
        OPERATIONS.put('*', (a, b) -> a * b);
        OPERATIONS.put('/', (a, b) -> a / b);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     *                  decimal mark, parentheses, operations signs '+', '-', '*',
     *                  '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is
     *         invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        String result = null;
        try {
            String postfixStatement = getPostfixFromInfix(statement);
            result = evaluatePostfix(postfixStatement);
        } catch (NoSuchElementException | IllegalArgumentException | ArithmeticException e) {
            return null;
        }
        if (result.indexOf('.') != -1) {
            result = MathUtils.roundHalfUp(Double.parseDouble(result), 0, DIGITS_AFTER_DOT);
        }
        return result;
    }

    private String getPostfixFromInfix(String statement) {
        if (statement == null) {
            throw new IllegalArgumentException("Statement cannot be null");
        }
        Deque<Character> stack = new ArrayDeque<>();
        StringBuilder result = new StringBuilder(statement.length());
        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            if (Character.isDigit(ch) || ch == '.') {
                result.append(ch);
            } else {
                if (!result.isEmpty()) {
                    result.append(' ');
                }
                if (ch == '(') {
                    stack.push(ch);
                } else if (ch == ')') {
                    char stackTail = stack.pop();
                    while (stackTail != '(') {
                        result.append(stackTail);
                        stackTail = stack.pop();
                    }
                } else if (isAllowedOperator(ch)) {
                    while (!stack.isEmpty()) {
                        char stackTail = stack.pop();
                        if (SIMBOLS_PRECEDENCE.get(stackTail) < SIMBOLS_PRECEDENCE.get(ch)) {
                            stack.push(stackTail);
                            break;
                        }
                        result.append(stackTail);
                    }
                    stack.push(ch);
                } else {
                    throw new IllegalArgumentException("Unknown symbols in statement");
                }
            }
        }
        while (!stack.isEmpty()) {
            char stackTail = stack.pop();
            if (stackTail == '(') {
                throw new IllegalArgumentException("Unmatched opening parenthesis");
            }
            result.append(stackTail);
        }
        return result.toString();
    }

    private boolean isAllowedOperator(char ch) {
        return OPERATIONS.containsKey(ch);
    }

    private String evaluatePostfix(String statement) {
        if (statement == null) {
            throw new IllegalArgumentException("Statement cannot be null");
        }
        Deque<String> stack = new ArrayDeque<>();
        StringBuilder multiDigitNumber = new StringBuilder();
        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            if (Character.isDigit(ch) || ch == '.') {
                multiDigitNumber.append(ch);
            } else if (ch == ' ') {
                if (!multiDigitNumber.isEmpty()) {
                    stack.push(multiDigitNumber.toString());
                    multiDigitNumber.setLength(0);
                }
            } else if (isAllowedOperator(ch)) {
                String rightOperand = null;
                if (!multiDigitNumber.isEmpty()) {
                    rightOperand = multiDigitNumber.toString();
                    multiDigitNumber.setLength(0);
                } else {
                    rightOperand = stack.pop();
                }
                String leftOperand = stack.pop();
                stack.push(evaluateBinaryOperation(leftOperand, rightOperand, ch));
            }
        }
        return stack.pop();
    }

    private String evaluateBinaryOperation(String leftOperand, String rightOperand, char operation) {
        double left = 0;
        double right = 0;
        try {
            left = Double.parseDouble(leftOperand);
            right = Double.parseDouble(rightOperand);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Operands cannot be null", e);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Operands were in inappropriate format", e);
        }
        double result = OPERATIONS.get(operation).apply(left, right);
        if (Double.isFinite(result)) {
            return Double.toString(result);
        } else {
            throw new ArithmeticException("Result of binary operation was infinite");
        }
    }

}
