package com.tsystems.javaschool.tasks;

import java.math.RoundingMode;
import java.text.NumberFormat;

public class MathUtils {

    public static double[] solveQuadraticInReal(double a, double b, double c) {
        if (a == 0) {
            throw new IllegalArgumentException("Not a quadratic equation as a was equal to 0");
        }
        double result[] = null;
        double determinant = b * b - 4 * a * c;
        if (determinant >= 0) {
            result = new double[2];
            result[0] = (-b + Math.sqrt(determinant)) / (2 * a);
            result[1] = (-b - Math.sqrt(determinant)) / (2 * a);
        }
        return result;
    }

    public static boolean isInteger(double number) {
        return (number == Math.floor(number)) && Double.isFinite(number);
    }

    public static String roundHalfUp(double value, int minFractionDigits, int maxFractionDigits) {
        if ((maxFractionDigits < 0) || (minFractionDigits < 0)) {
            throw new IllegalArgumentException("Number of fraction digits cannot be negative");
        }
        if (minFractionDigits > maxFractionDigits) {
            throw new IllegalArgumentException("Max cannot be less than min");
        }
        NumberFormat roundingFormat = NumberFormat.getInstance();
        roundingFormat.setMaximumFractionDigits(maxFractionDigits);
        roundingFormat.setMinimumFractionDigits(minFractionDigits);
        roundingFormat.setRoundingMode(RoundingMode.HALF_UP);
        return roundingFormat.format(value);
    }
}
