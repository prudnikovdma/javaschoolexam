package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import com.tsystems.javaschool.tasks.MathUtils;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and
     * maximum at the bottom, from left to right). All vacant positions in the array
     * are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build
     *                with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) {
            throw new CannotBuildPyramidException("List of input numbers cannot be null");
        }
        if (inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException("List of input numbers cannot be empty");
        }
        int result[][] = null;
        try {
            int rowsNumber = getRowsNumber(inputNumbers.size());
            List<Integer> sortedInput = new ArrayList<>(inputNumbers);
            Collections.sort(sortedInput);
            result = buildPyramide(sortedInput, rowsNumber);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException("Source values cannot be null", e);
        } catch (IllegalArgumentException e) {
            throw new CannotBuildPyramidException(e);
        }
        return result;
    }

    private int getRowsNumber(int inputSize) {
        // In order to build a symmetrical pyramid we need inputSize be a sum
        // of arithmetic progression, because in the first row of the pyramid
        // we place one number, on the second row we place two numbers and so on.
        // Therefore, to find out if size of the list is sum of some arithmetic
        // progression we take equation for sum of arithmetical progression:
        // sum = (n * (2 * a + (n - 1) * d)) / 2
        // where sum is sum of arithmetical progression, n is the number of terms,
        // a is the first term and d is the difference between terms,
        // and transform that equation to solve for n:
        // (n * n * d) / 2 + n * (a - d / 2) - sum = 0
        // In case of building a symmetrical pyramid a = 1 d = 1
        // (n * n) / 2 + n / 2 - sum = 0
        // If we substitute sum with .size() and get integral n, then
        // inputSize is a sum of arithmetic progression and we can build a symmterical
        // pyramid.
        double res[] = MathUtils.solveQuadraticInReal(0.5, 0.5, -inputSize);
        // With a = 0.5, b = 0.5 and .size() returning number >= 1
        // determinant is always >= 2.25, sqrt(determinant) >= 1.5
        // one root is positive and one root is negative
        if ((res == null) || (res.length < 2) || ((res[0] <= 0) && (res[1] <= 0))) {
            // shouldn't happen
            throw new CannotBuildPyramidException("Invalid calculation of pyramid's rows number");
        }
        double presumableRowsNumber = res[0] > res[1] ? res[0] : res[1];
        if (!MathUtils.isInteger(presumableRowsNumber)) {
            throw new IllegalArgumentException("Unsuitable input size for symmetrical pyramid");
        }
        return (int) presumableRowsNumber;
    }

    private int[][] buildPyramide(List<Integer> sortedValues, int rowsNumber) {
        if (sortedValues == null) {
            throw new IllegalArgumentException("Source of values cannot be null");
        }
        int result[][] = null;
        try {
            result = new int[rowsNumber][];
        } catch (OutOfMemoryError e) {
            throw new IllegalArgumentException("Number of rows was too high", e);
        }
        int rowLength = (2 * rowsNumber) - 1;
        int middle = rowLength / 2;
        Iterator<Integer> it = sortedValues.iterator();
        for (int i = 0; i < rowsNumber; i++) {
            try {
                result[i] = new int[rowLength];
                for (int j = middle - i; j <= middle + i; j += 2) {
                    result[i][j] = it.next();
                }
            } catch (OutOfMemoryError e) {
                throw new IllegalArgumentException("Too many values to build pyramid from", e);
            } catch (NoSuchElementException e) {
                throw new IllegalArgumentException("Number of rows doesn't match amount of values", e);
            }
        }
        return result;
    }

}
