package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    public CannotBuildPyramidException() {
        super();
    }

    public CannotBuildPyramidException(String message) {
        super(message);
    }

    public CannotBuildPyramidException(Throwable cause) {
        super(cause);
    }

    public CannotBuildPyramidException(String message, Throwable cause) {
        super(message, cause);
    }
}
